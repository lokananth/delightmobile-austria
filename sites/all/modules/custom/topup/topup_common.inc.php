<?php

/**
 * Function to API call using Curl lib
 * @param type $arr_param
 * @return type
 */
 
 	
 define('DMSE_MYACCOUNT_TOPUP_AMOUNT', 'http://192.168.2.102:9706/api/v1/TopupAmount');
 define('DMSE_MYACCOUNT_TOPUP_STEP1', 'http://192.168.2.102:9706/api/v1/CTACCTopupStep1');
 define('DMSE_MYACCOUNT_TOPUP_STEP2', 'http://192.168.2.102:9706/api/v1/CTACCTopupStep2');
 define('DMSE_MYACCOUNT_TOPUP_ADDCREDIT', 'http://192.168.2.102:9706/api/v1/TopupProcessByCreditCard');
 define('DMSE_MYACCOUNT_TOPUP_PAYMENT_LOG', 'http://192.168.2.102:9706/api/v1/ThreeDSPaymentLog');
 define('DMSE_MYACCOUNT_TOPUP_STATUS', 'http://192.168.2.102:9706/api/v1/GetTopupLog');
 define('DMSE_MYACCOUNT_AUTO_TOPUP', 'http://192.168.2.102:9706/api/v1/UpdateAutoTopupStatus');
 define('DMSE_MYACCOUNT_PAYMENT_UPDATE', 'http://192.168.2.102:9706/api/v1/Insertmvnopaymenttransaction');
 
 define('DMSE_MYACCOUNT_TOPUP_STATUS_SMS', 'http://192.168.2.102:9706/api/v1/SendTopupStatusSMS');
  
 //Auto Topup API
 define('DMSE_MYACCOUNT_ADD_NEW_ORDER', 'http://192.168.2.102:9706/api/v1/AddNewOrder');
 define('DMSE_MYACCOUNT_PAY_AUTO_TOPUP', 'http://192.168.2.102:9706/api/v1/PayAutoTopup');
 define('DMSE_MYACCOUNT_PAY_AUTO_TOPUP_LOG', 'http://192.168.2.102:9706/api/v1/PayAutoTopupLog');
 define('DMSE_MYACCOUNT_DELETE_AUTO_TOPUP', 'http://192.168.2.102:9706/api/v1/DeleteAutoTopup');
 
 
 function apiGetKey2($username) {
	$isAuth = true;
    //$headers[0] = 'Content-MD5: 917200022538';
    $headers[0] = 'Content-MD5: ' . $username;
	$headers[1] = 'User-Agent: mundiovectone';
	$headers[2] = 'Accept: application/json';
	$headers[3] = 'Accept-Charset: UTF-8';
	$headers[4] = 'Content-Type: application/json';
	$headers[5] = 'Host: 192.168.2.102:9706';
	$headers[6] = 'Content-Length: 0';
	
	if (!$isAuth){
		$headers = array();
	}
	
	$apiUrl = config_item('vrp_api_endpoint')."Key";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	if ($isAuth){
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}
    $response = curl_exec($ch);
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
    return $response;
}


function apiGet2($apiUrl, $isAuth = true) {
    $headers = array(
        'Authorization: mundiovectone F2SeWBiCUvVmO6z66C6qgXNAoHNe4YB9aIW3nK78zxw=',
        'Host: webapi.vectone.com'
    );
	if (!$isAuth){
		$headers = array();
	}
	

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	if ($isAuth){
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}
    $response = curl_exec($ch);
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
    return $response;
}

function apiPost2($apiUrl, $data = Array(), $isAuth = true) {

	$header[0] = 'Authorization: mundiovectone F2SeWBiCUvVmO6z66C6qgXNAoHNe4YB9aIW3nK78zxw=';
	$header[1] = 'Mundio-Api-PublicKey: OTE3MjAwMDIyNTM4';
	$header[2] = 'Content-MD5: 917200022538';
	$header[3] = 'User-Agent: mundiovectone';
	$header[4] = 'Accept: application/json';
	$header[5] = 'Accept-Charset: UTF-8';
	$header[6] = 'Content-Type: application/json';
	$header[7] = 'Host: 192.168.2.102:9706';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
	if ($data) {
		$str = json_encode($data);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
	}
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	
	//echo "<pre>";
	//print_r($header);
	//exit;
    $output = curl_exec($ch);
	curl_close($ch);
    return json_decode($output, true);
}

function myaccount_curl_get_contents2($arr_param) {

    $url = $arr_param['url'];
    // Initiate the curl session
    $ch = curl_init();
    // Set the URL
    curl_setopt($ch, CURLOPT_URL, $url);
    // Removes the headers from the output
    curl_setopt($ch, CURLOPT_HEADER, 0);
    // Return the output instead of displaying it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Execute the curl session
    $output = curl_exec($ch);
    // Close the curl session
    curl_close($ch);
    // Return the output as a variable 

    return $output;
}

function myaccount_curl_post_contents2($arr_param) {/* echo '<pre>'; print_r($arr_param);*/
   
    $apiURL = $arr_param['url'];
    $apiValue = $arr_param['post_value'];
    header("HTTP/1.1 200 OK");
    header("Content-Type: application/json");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiValue));
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $output = curl_exec($ch);
    curl_close($ch);

    return json_decode($output, true);
}


