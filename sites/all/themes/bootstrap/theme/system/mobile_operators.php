<b>Supported Destination</b><br>
With Delight Credit Transfer, you can send credit to international mobile numbers: <br><br>

<select style="width:170px;" id="myselect" onchange="mobile_operators();" name="dropdowncountry">
	<option value="Please Select" selected="selected">Please  Select</option>
	<option value="AFG">Afghanistan</option>
	<option value="ALB">Albania</option>
	<option value="AIA">Anguilla</option>
	<option value="ATG">Antigua and Barbuda</option>
	<option value="ARG">Argentina</option>
	<option value="ARM">Armenia</option>
	<option value="ABW">Aruba</option>
	<option value="BGD">Bangladesh</option>
	<option value="BRB">Barbados</option>
	<option value="BEN">Benin</option>
	<option value="BOL">Bolivia</option>
	<option value="BRA">Brazil</option>
	<option value="VGB">British Virgin Islands</option>
	<option value="BFA">Burkina Faso</option>
	<option value="BDI">Burundi</option>
	<option value="KHM">Cambodia</option>
	<option value="CMR">Cameroon</option>
	<option value="CYM">Cayman Islands</option>
	<option value="CAF">Central African Republic</option>
	<option value="CHL">Chile</option>
	<option value="CHN">China</option>
	<option value="COL">Colombia</option>
	<option value="COG">Congo</option>
	<option value="CRI">Costa Rica</option>
	<option value="CUB">Cuba</option>
	<option value="CYP">Cyprus</option>
	<option value="DMA">Dominica</option>
	<option value="DOM">Dominican Republic</option>
	<option value="ECU">Ecuador</option>
	<option value="EGY">Egypt</option>
	<option value="SLV">El Salvador</option>
	<option value="FJI">Fiji</option>
	<option value="FRA">France</option>
	<option value="GMB">Gambia</option>
	<option value="GHA">Ghana</option>
	<option value="GRD">Grenada</option>
	<option value="GTM">Guatemala</option>
	<option value="GNB">Guinea Bissau</option>
	<option value="GIN">Guinea Republic</option>
	<option value="GUY">Guyana</option>
	<option value="HTI">Haiti</option>
	<option value="HND">Honduras</option>
	<option value="IND">India</option>
	<option value="IDN">Indonesia</option>
	<option value="IRQ">Iraq</option>
	<option value="CIV">Ivory Coast</option>
	<option value="JAM">Jamaica</option>
	<option value="JOR">Jordan</option>
	<option value="KAZ">Kazakhstan</option>
	<option value="KEN">Kenya</option>
	<option value="LAO">Laos</option>
	<option value="LBR">Liberia</option>
	<option value="MDG">Madagascar</option>
	<option value="MYS">Malaysia</option>
	<option value="MLI">Mali</option>
	<option value="MEX">Mexico</option>
	<option value="MSR">Montserrat</option>
	<option value="MAR">Morocco</option>
	<option value="MOZ">Mozambique</option>
	<option value="NPL">Nepal</option>
	<option value="NIC">Nicaragua</option>
	<option value="NER">Niger</option>
	<option value="NGA">Nigeria</option>
	<option value="PAK">Pakistan</option>
	<option value="PSE">Palestine</option>
	<option value="PAN">Panama</option>
	<option value="PNG">Papua New Guinea</option>
	<option value="PRY">Paraguay</option>
	<option value="PER">Peru</option>
	<option value="PHL">Philippines</option>
	<option value="POL">Poland</option>
	<option value="PRI">Puerto Rico</option>
	<option value="ROU">Romania</option>
	<option value="RUS">Russia</option>
	<option value="RWA">Rwanda</option>
	<option value="SEN">Senegal</option>
	<option value="SGP">Singapore</option>
	<option value="SOM">Somalia</option>
	<option value="ZAF">South Africa</option>
	<option value="ESP">Spain</option>
	<option value="LKA">Sri Lanka</option>
	<option value="KNA">St Kitts and Nevis</option>
	<option value="LCA">St Lucia</option>
	<option value="VCT">St Vincent Grenadines</option>
	<option value="SDN">Sudan</option>
	<option value="SUR">Suriname</option>
	<option value="SWZ">Swaziland</option>
	<option value="SYR">Syria</option>
	<option value="TZA">Tanzania</option>
	<option value="THA">Thailand</option>
	<option value="TGO">Togo</option>
	<option value="TTO">Trinidad and Tobago</option>
	<option value="TUN">Tunisia</option>
	<option value="TUR">Turkey</option>
	<option value="TCA">Turks and Caicos</option>
	<option value="UGA">Uganda</option>
	<option value="UKR">Ukraine</option>
	<option value="USA">United States</option>
	<option value="URY">Uruguay</option>
	<option value="VNM">Vietnam</option>
	<option value="YEM">Yemen</option>
	<option value="ZMB">Zambia</option>
	<option value="ZWE">Zimbabwe</option>

</select>

<span id="operator_list"></span>

<script>

function mobile_operators(){


var ccode=$( "#myselect option:selected" ).val();
	$.ajax({
	        url: "/sites/all/themes/bootstrap/theme/system/mobileoperator_rates_result.php?cname="+ccode,
	        type: "POST",
	        data: ({name:ccode}),
	        success: function(data){

	        	//alert("success");

				 if(data) {  
				  
					$('#operator_list').html(data);
				       }
				else { 
				// DO SOMETHING 
				       }
		}
	    });

}


</script>